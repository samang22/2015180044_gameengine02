﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;

public class TileSelect : MonoBehaviour
{
    public Camera _mainCamera;
    public Tilemap _tileMap;
    private Vector3 _clickPos = Vector3.zero;
    private Vector3Int _tilemapCell = Vector3Int.zero;
    private Vector2 mouseClick = Vector2.zero;




    IEnumerator OnMouseUp()
    {
        if (EventSystem.current.IsPointerOverGameObject() == true)
        {
            yield break;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

        Vector3 scrSpace = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 tilePos = _mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, scrSpace.z)) - transform.position;

        _tilemapCell = _tileMap.LocalToCell(tilePos);
        _clickPos = _tileMap.GetCellCenterLocal(_tilemapCell);
        Vector3 tileWorldPos = _tileMap.CellToWorld(_tilemapCell);
        print(_clickPos.x + ", " + _clickPos.y);

        GameObject go;
        TowerScript ts;
        switch (GameManager.Instance.gameInfo.TowerSelect)
        {
            case 0:
                break;
            case 1:
                //GameManager.Instance.gameInfo.TowerSelect = 0;

                if (GameManager.Instance.gameInfo.Money > 200)
                {
                    GameManager.Instance.gameInfo.Money -= 200;
                    go = ObjectPoolManager.Instance.Spawn("Tower01");
                    go.transform.position = tileWorldPos;
                    Vector3 tempV = go.transform.position;
                    tempV.x += 0.5f;
                    tempV.y += 0.5f;
                    go.transform.position = tempV;

                    ts = go.GetComponent<TowerScript>();
                    ts.missileName = "missile01";
                    ts.Init();
                }

                break;
            case 2:
                //GameManager.Instance.gameInfo.TowerSelect = 0;
                if (GameManager.Instance.gameInfo.Money > 500)
                {
                    GameManager.Instance.gameInfo.Money -= 500;
                    go = ObjectPoolManager.Instance.Spawn("Tower02");
                    go.transform.position = tileWorldPos;
                    Vector3 tempV = go.transform.position;
                    tempV.x += 0.5f;
                    tempV.y += 0.5f;
                    go.transform.position = tempV;

                    ts = go.GetComponent<TowerScript>();
                    ts.missileName = "missile02";
                    ts.Init();

                }
                break;
            case 3:
                //GameManager.Instance.gameInfo.TowerSelect = 0;
                if (GameManager.Instance.gameInfo.Money > 1000)
                {
                    GameManager.Instance.gameInfo.Money -= 1000;
                    go = ObjectPoolManager.Instance.Spawn("Tower03");
                    go.transform.position = tileWorldPos;
                    Vector3 tempV = go.transform.position;
                    tempV.x += 0.5f;
                    tempV.y += 0.5f;
                    go.transform.position = tempV;

                    ts = go.GetComponent<TowerScript>();
                    ts.missileName = "missile03";
                    ts.Init();
                }
                break;
            default:
                break;
        }


        yield return null;
    }


    //private void onMouseOver()
    //{
    //    Debug.Log("Over");
    //    print("Over");
    //}

    private void OnMouseOver()
    {
        Debug.Log("Over");
        print("Over");
    }

    private void onMouseClick()
    {
        Debug.Log("Click");
        print("Click");

        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //Debug.DrawRay(ray.origin, ray.direction * 10, Color.blue, 3.5f);

        //RaycastHit2D hit = Physics2D.Raycast(ray.origin, Vector3.zero);

        //if (_tileMap = hit.transform.GetComponent<Tilemap>())
        //{
        //    _tileMap.RefreshAllTiles();

        //    int x, y;
        //    x = _tileMap.WorldToCell(ray.origin).x;
        //    y = _tileMap.WorldToCell(ray.origin).y;

        //    Vector3Int v3Int = new Vector3Int(x, y, 0);


        //    _tileMap.SetTileFlags(v3Int, TileFlags.None);
        //    _tileMap.SetColor(v3Int, (Color.red));
        //}
    }
    //private void onMouseExit()
    //{
    //    this.tilemap.RefreshAllTiles();
    //}


    private void Update()
    {
        //if (Input.GetMouseButtonUp(0))
        //{
        //    mouseClick = Input.mousePosition;
        //    mouseClick = _mainCamera.ScreenToWorldPoint(mouseClick);
        //    if (mouseClick.x < -0.5f || mouseClick.x > 9.5f)
        //    {
        //        return;
        //    }
        //    GameObject go;
        //    TowerScript ts;
        //    switch (GameManager.Instance.gameInfo.TowerSelect)
        //    {
        //        case 0:
        //            break;
        //        case 1:
        //            //GameManager.Instance.gameInfo.TowerSelect = 0;
        //            if (GameManager.Instance.gameInfo.Money > 200)
        //            {
        //                GameManager.Instance.gameInfo.Money -= 200;
        //                go = ObjectPoolManager.Instance.Spawn("Tower01");
        //                go.transform.position.Set(mouseClick.x, mouseClick.y, 0.0f);
        //                ts = go.GetComponent<TowerScript>();
        //                ts.missileName = "missile01";
        //                ts.Init();
        //            }
        //            break;
        //        case 2:
        //            //GameManager.Instance.gameInfo.TowerSelect = 0;
        //            if (GameManager.Instance.gameInfo.Money > 500)
        //            {
        //                GameManager.Instance.gameInfo.Money -= 500;
        //                go = ObjectPoolManager.Instance.Spawn("Tower02");
        //                go.transform.position.Set(mouseClick.x, mouseClick.y, 0.0f);
        //                ts = go.GetComponent<TowerScript>();
        //                ts.missileName = "missile02";
        //                ts.Init();
        //            }
        //            break;
        //        case 3:
        //            //GameManager.Instance.gameInfo.TowerSelect = 0;
        //            if (GameManager.Instance.gameInfo.Money > 1000)
        //            {
        //                GameManager.Instance.gameInfo.Money -= 1000;
        //                go = ObjectPoolManager.Instance.Spawn("Tower03");
        //                go.transform.position.Set(mouseClick.x, mouseClick.y, 0.0f);
        //                ts = go.GetComponent<TowerScript>();
        //                ts.missileName = "missile03";
        //                ts.Init();
        //            }
        //            break;
        //        default:
        //            break;
        //    }
        //}
    }
}