﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuOpenButtonUI : MonoBehaviour
{

    private void Start()
    {
        EventManager.On("game_started", OnGameStart);
        EventManager.On("game_paused", OnGamePause);
        gameObject.SetActive(false);
    }

    public void Clicked()
    {
        EventManager.Emit("game_paused", null);
    }

    private void OnGameStart(object obj)
    {
        gameObject.SetActive(true);
    }
    private void OnGamePause(object obj)
    {
        gameObject.SetActive(false);
    }
}
