﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBarUI : MonoBehaviour
{
    public TowerScript tower;

    private Slider _slider;

    private void Awake()
    {
        _slider = GetComponent<Slider>();
    }
    // Start is called before the first frame update
    void Start()
    {
        EventManager.On("game_started", OnGameStart);
        EventManager.On("game_ended", OnGameEnded);
        gameObject.SetActive(false);
    }

    private void OnGameStart(object obj)
    {
        gameObject.SetActive(true);
    }
    private void OnGameEnded(object obj)
    {
        gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        //_slider.value = tower.Health / tower.maxHealth;
    }
}
