﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public float spawnRate = 2;
    public string spawnTargetName = "virus";

    private float timeSpend = 0.0f;
    private float levelUp = 2.0f;


    private Coroutine _spawnRoutine;

    void Start()
    {
        EventManager.On("game_started", OnGameStart);
        EventManager.On("game_paused", OnGamePause);
        EventManager.On("game_ended", OnGameEnded);
    }

    private void OnGameStart(object obj)
    {
        _spawnRoutine = StartCoroutine(SpawnRoutine());
    }
    private void OnGamePause(object obj)
    {
        StopCoroutine(_spawnRoutine);
    }
    private void OnGameEnded(object obj)
    {
        StopCoroutine(_spawnRoutine);
    }
    private IEnumerator SpawnRoutine()
    {
        while (true)
        {
            var go = ObjectPoolManager.Instance.Spawn(spawnTargetName);
            go.transform.position = transform.position;
            go.transform.rotation = transform.rotation;
            //go.transform.localScale = transform.localScale;

            yield return new WaitForSeconds(spawnRate);
        }
    }

    private void Update()
    {
        if (timeSpend > levelUp)
        {
            levelUp += 2.0f;
            timeSpend = 0.0f;

            spawnRate -= 0.1f;
            if (spawnRate < 0.2f)
            {
                spawnRate = 0.2f;
            }
        }
    }
}
