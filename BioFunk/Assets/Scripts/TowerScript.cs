﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScript : MonoBehaviour
{
    public string missileName;
    public float throwRate = 0.5f;
    private float timeSpend = 0.0f;
    private Coroutine _throwRoutine;


    public bool _IsVirus = false;
    public Vector3 VirusPos = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        EventManager.On("game_started", OnGameStart);
        EventManager.On("game_paused", OnGamePaused);
        EventManager.On("game_ended", OnGameEnded);
    }
    private void OnGameStart(object obj)
    {
        gameObject.SetActive(true);
        //_throwRoutine = StartCoroutine(ShootRoutine());
    }
    private void OnGameEnded(object obj)
    {
        //StopCoroutine(_throwRoutine);
        gameObject.SetActive(false);
    }
    private void OnGamePaused(object obj)
    {
        //StopCoroutine(_throwRoutine);
        gameObject.SetActive(true);
    }



    private IEnumerator ShootRoutine()
    {


        //while (true)
        //{
        //    var go = ObjectPoolManager.Instance.Spawn(missileName);
        //    go.transform.position = transform.position;
        //    go.transform.rotation = transform.rotation;
        //    yield return new WaitForSeconds(throwRate);
        //}

        return null;
    }

    // Update is called once per frame
    void Update()
    {
        timeSpend += Time.deltaTime;

        if (_IsVirus)
        {
            _IsVirus = false;
            if (timeSpend > throwRate)
            {
                timeSpend = 0.0f;
                GameObject go = ObjectPoolManager.Instance.Spawn(missileName);
                go.transform.position = transform.position;

                Missile missile = go.GetComponent<Missile>();
                missile.Dir = (VirusPos - transform.position).normalized;

            }


            VirusPos = Vector3.zero;

            
        }


    }

    private void Show(object obj) => gameObject.SetActive(true);
    private void Hide(object obj) => gameObject.SetActive(false);


    public void Init()
    {
        //EventManager.On("game_started", OnGameStart);
        //EventManager.On("game_paused", OnGamePaused);
        //EventManager.On("game_ended", OnGameEnded);
        gameObject.SetActive(true);
        //_throwRoutine = StartCoroutine(ShootRoutine());
    }

}
