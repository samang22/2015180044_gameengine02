﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusController : MonoBehaviour
{
    public float speed = 3f;
    public float maxHeath = 2f;
    private float _health;
    private Rigidbody2D _rigidbody2D;

    private AStar _aStar;
    private int NodeIndex;


    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        //_aStar = GetComponent<AStar>();
    }

    private void OnEnable()
    {
        _health = maxHeath;


        // 노드 인덱스 초기화
        NodeIndex = 1;

        _aStar = new AStar();

        // 좌하, 우상, 시작, 타겟 초기화

        _aStar.bottomLeft.x = 0; _aStar.bottomLeft.y = 0;
        _aStar.topRight.x = 9; _aStar.topRight.y = 9;
        _aStar.startPos.x = 0; _aStar.startPos.y = 0;
        switch (GameManager.Instance.gameInfo.MapNum)
        {
            case Map.Map01:
                _aStar.targetPos = GameManager.Instance.gameInfo.Map01TargetPos;
                break;
            case Map.Map02:
                _aStar.targetPos = GameManager.Instance.gameInfo.Map02TargetPos;
                break;
            case Map.Map03:
                _aStar.targetPos = GameManager.Instance.gameInfo.Map03TargetPos;
                break;
            default:
                break;
        }
        // 대각 여부 초기화
        _aStar.allowDiagonal = false; _aStar.dontCrossCorner = false;




        _aStar.PathFinding();
    }

    private void Start()
    {
        EventManager.On("game_ended", OnGameEnded);


    }

    private void OnGameEnded(object obj)
    {
        gameObject.SetActive(false);
    }

    void Update()
    { 
        if (GameManager.Instance.state == GameState.Paused)
        {
            return;
        }
        if (transform.position.x >= _aStar.FinalNodeList[NodeIndex].x - 0.1
            && transform.position.x <= _aStar.FinalNodeList[NodeIndex].x + 0.1
            && transform.position.y >= _aStar.FinalNodeList[NodeIndex].y - 0.1
            && transform.position.y <= _aStar.FinalNodeList[NodeIndex].y + 0.1)
        {
            ++NodeIndex;
        }

        if (NodeIndex >= _aStar.FinalNodeList.Count - 1)
        {
            NodeIndex = _aStar.FinalNodeList.Count - 1;
            _rigidbody2D.velocity = Vector2.right * speed;
            return;
        }

        Vector2 target;

        target.x = _aStar.FinalNodeList[NodeIndex].x;
        target.y = _aStar.FinalNodeList[NodeIndex].y;

        Vector2 vel;
        vel.x = target.x - transform.position.x;
        vel.y = target.y - transform.position.y;
        vel.Normalize();

        _rigidbody2D.velocity = vel * speed;





    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Heart"))
        {
            var heart = other.GetComponent<Heart>();
            heart.Damage(1f);
            gameObject.SetActive(false);
        } else if (other.CompareTag("Missile"))
        {
            _health -= 1f;
            other.gameObject.SetActive(false);
            if (_health <= 0)
            {
                GameManager.Instance.gameInfo.Money += 10;
                gameObject.SetActive(false);
            }
        }
    }
}
