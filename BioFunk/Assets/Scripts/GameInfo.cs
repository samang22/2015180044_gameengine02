﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Map
{
    Map01,
    Map02,
    Map03
}

public enum eBoardInfo
{
    Empty,
    Road,
    Tower
}

public class GameInfo/* : SingletonBehavior<GameManager>*/
{
    public Map MapNum;
    public Vector2Int Map01TargetPos;
    public Vector2Int Map02TargetPos;
    public Vector2Int Map03TargetPos;

    public eBoardInfo[,] BoardInfo;
    public int Money;
    public int TowerSelect;



    public void Init()
    {

        Money = 200000;
        TowerSelect = 1;


        this.MapNum = Map.Map01;
        this.Map01TargetPos.x = 9; this.Map01TargetPos.y = 1;
        this.Map02TargetPos.x = 9; this.Map02TargetPos.y = 1;
        this.Map03TargetPos.x = 9; this.Map03TargetPos.y = 1;

        this.BoardInfo = new eBoardInfo[10, 10]
        {
            { eBoardInfo.Empty, eBoardInfo.Empty,eBoardInfo.Empty,eBoardInfo.Empty,eBoardInfo.Empty,eBoardInfo.Empty,eBoardInfo.Empty,eBoardInfo.Empty,eBoardInfo.Empty,eBoardInfo.Empty },
            { eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road },
            { eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road } ,
            { eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty },
            { eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Empty},
            { eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty},
            { eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Empty},
            { eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty},
            { eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Road, eBoardInfo.Empty},
            { eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Empty, eBoardInfo.Road, eBoardInfo.Empty }
        };

    }
}
