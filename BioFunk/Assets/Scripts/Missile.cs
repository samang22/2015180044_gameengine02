﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Missile : MonoBehaviour
{
    public float speed = 4;
    private Rigidbody2D _rigidbody2D;
    public string missileName;
    public Vector3 Dir;

    private void Awake()
    {
    }
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.state == GameState.Paused)
        {
            return;
        }

        if (speed < 3)
        {
            speed = 3;
        }

        //_rigidbody2D.AddForce(Dir * speed);

        _rigidbody2D.velocity = Dir * speed;

        if (transform.position.x < - 500.0f
            || transform.position.x > 500.0f
            || transform.position.y < -500.0f
            || transform.position.y > 500.0f

            )
        {
            gameObject.SetActive(false);
        }

    }
}
