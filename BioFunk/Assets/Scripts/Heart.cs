﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    public float maxHealth = 10;
    private float _health;
    public float Health => _health;



    void Start()
    {
        EventManager.On("game_started", OnGameStart);
        EventManager.On("game_paused", OnGamePaused);
        EventManager.On("game_ended", OnGameEnded);
    }

    private void OnGameStart(object obj)
    {
        _health = maxHealth;
        gameObject.SetActive(true);
    }
    private void OnGameEnded(object obj)
    {
        gameObject.SetActive(false);
    }
    private void OnGamePaused(object obj)
    {
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void Show(object obj) => gameObject.SetActive(true);
    private void Hide(object obj) => gameObject.SetActive(false);

    public void Damage(float damage)
    {
        _health -= damage;
        if (_health < 0)
        {
            EventManager.Emit("game_ended", null);
        }
    }

}
