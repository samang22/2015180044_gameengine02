﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonBehavior<GameManager>
{
    public GameState state;
    public GameInfo gameInfo;
    private void Awake()
    {
        gameInfo = new GameInfo();
        gameInfo.Init();
    }
    private void Start()
    {
        EventManager.On("game_started", OnGameStart);
        EventManager.On("game_paused", OnGamePause);
        EventManager.On("game_ended", OnGameEnded);
    }


    private void OnGameStart(object obj) => state = GameState.Playing;
    private void OnGamePause(object obj) => state = GameState.Paused;
    private void OnGameEnded(object obj) => state = GameState.Ended;





}


