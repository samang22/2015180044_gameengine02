﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class ObjectPoolManager : SingletonBehavior<ObjectPoolManager>
{
    [Serializable]
    public class ObjectPoolData
    {
        public string name;
        public GameObject prefab;
    }

    public List<ObjectPoolData> prefabs;

    private IDictionary<string, List<GameObject>> _objectPool;
    private void Awake()
    {
        _objectPool = new Dictionary<string, List<GameObject>>();
    }

    public GameObject Spawn(string spawnTargetName)
    {
        if (_objectPool.ContainsKey(spawnTargetName) == false)
        {
            _objectPool.Add(spawnTargetName, new List<GameObject>());
        }
        var founded = _objectPool[spawnTargetName]
            .FirstOrDefault(go => !go.activeInHierarchy);

        if (founded == null)
        {
            var foundedPrefabData 
                = prefabs.FirstOrDefault(prefabData => prefabData.name == spawnTargetName);

            if (foundedPrefabData == null)
            {
                Debug.LogWarning($"{spawnTargetName} 이라는 prefab은 존재하지 않습니다.");
                return null;
            }

            founded = Instantiate(foundedPrefabData.prefab);
            _objectPool[spawnTargetName].Add(founded);
        }

        founded.SetActive(true);
        return founded;
    }
}
