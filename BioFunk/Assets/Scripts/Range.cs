﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Range : MonoBehaviour
{
    public GameObject _tower;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        int a = 0;
        a += 5;
        if (other.CompareTag("Virus"))
        {
            TowerScript ts = _tower.GetComponent<TowerScript>();
            ts._IsVirus = true;
            Rigidbody2D virusRb = other.GetComponent<Rigidbody2D>();
            ts.VirusPos = virusRb.position;
        }
    }
}

